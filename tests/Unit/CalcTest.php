<?php

namespace Tests\Unit;

use App\Calc;
use Tests\TestCase;

class Math {

}

class CalcTest extends TestCase {

    public function test_can_add_two_numbers() {
        $calc = new Calc();
        $result = $calc->add(1, 5);

        $this->assertEquals(6, $result);
    }

    public function test_can_add_three_numbers() {
        $calc = new Calc();
        $result = $calc->add(1, 5, 2);

        $this->assertEquals(8, $result);
    }
}
